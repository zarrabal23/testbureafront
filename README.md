Test Burea Project
---------------------------------------------------

**Projects Dependencies**
---------------------------------------------------

**Node Js**
Install Node js downloading it from the following link using the LTS version
https://nodejs.org/en/

---------------------------------------------------
**Angular CLI**
Para Instalar Angular cli, lo primero que debemos hacer es abrir una linea de comando de windows o linux y ejecutar el siguiente comando.
```
npm install -g @angular/cli
```

What this command will do is to install the CLI globally using the node npm js

---------------------------------------------------
*** Clone the repository ***
```
git clone https://zarrabal23@bitbucket.org/zarrabal23/testbureafront.git
```

---------------------------------------------------
*** Insta Dependencies of the project ***
Para instalar las dependencias del proyecto nos debemos posicionar en la carpeta del repositorio previamente clonado y ejecutamos el siguiente comando:

```
npm install
```

---------------------------------------------------

*** Execute Instance ***

Once we have finished downloading all the dependencies we proceed to execute the following command to raise our application:

ng serve -o