export const AppConstants =
{
    CONTENT_TYPE_URL_ENCODED: 'application/x-www-form-urlencoded',
    CONTENT_TYPE_JSON: 'application/json',

    JSON: 'json',

};
