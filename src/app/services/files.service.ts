import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileMongo } from '../model/FileMongo';
import { environment } from 'src/environments/environment';
import { AppConstants } from '../utilities/AppConstants';
import { MarkDown } from '../model/MarkDown';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private http: HttpClient) {}

  /**
   * Method used to get all files
   */
  public getFiles(): Observable<Array<FileMongo>> {
        const url = environment.API_URL + environment.URL_FILES;

        /** Add headres to request */

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON,
            }),
            responseType: 'json' as 'json',
        };

        return this.http.get<Array<FileMongo>>(url, httpOptions);
    }

  /**
   * Method used to get all files
   */
  public getFileDetail(id: string): Observable<FileMongo> {
        const url = environment.API_URL + environment.URL_FILES + 'detail';

        /** Add headres to request */

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON,
            }),
            responseType: 'json' as 'json',
            params: {
                fileId: id
            }
        };

        return this.http.get<FileMongo>(url, httpOptions);
    }

    /**
     *
     */
    public getAllDocuments(): Observable<Array<MarkDown>> {
        const url = environment.API_URL + environment.URL_FILES + 'alldocuments';

        /** Add headres to request */

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON,
            }),
            responseType: 'json' as 'json',
        };

        return this.http.get<Array<MarkDown>>(url, httpOptions);
    }

    /**
     * Method used to save record in database
     * 
     * @param markDown 
     */
    public save(markDown: MarkDown): Observable<Array<MarkDown>> {
        const url = environment.API_URL + environment.URL_FILES + 'save';

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON
            }),
            responseType: 'json' as 'json'
        };

        return this.http.post<Array<MarkDown>>(url, markDown, httpOptions);
    }

    /**
     * Method used to update record in database
     * 
     * @param markDown 
     */
    public update(markDown: MarkDown): Observable<Array<MarkDown>> {
        const url = environment.API_URL + environment.URL_FILES + 'update/' + markDown.id;

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON
            }),
            responseType: 'json' as 'json'
        };

        return this.http.put<Array<MarkDown>>(url, markDown, httpOptions);
    }

    /**
     * Method used to delete record in database
     * 
     * @param id
     */
    public delete(id: string): Observable<Array<MarkDown>> {
        const url = environment.API_URL + environment.URL_FILES + id;

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': AppConstants.CONTENT_TYPE_JSON
            }),
            responseType: 'json' as 'json'
        };

        return this.http.delete<Array<MarkDown>>(url, httpOptions);
    }
}
