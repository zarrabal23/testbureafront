export class FileMongo {
    id: string;
    fileName: string;
    contentType: string;
    fileBase64: string;
}
