export class MarkDown {
    id: string;
    fileName: string;
    content: string;
}
