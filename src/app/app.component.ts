import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FilesService } from './services/files.service';
import { HttpErrorResponse } from '@angular/common/http';
import { FileMongo } from './model/FileMongo';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MarkDown } from './model/MarkDown';
import { MarkdownComponent } from 'ngx-markdown';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    title = 'test-file';
    files: Array<FileMongo>;
    public file: FileMongo;

    documents: Array<MarkDown>;
    documentSelected: MarkDown;

    markdown = `## __Editor__!`;

    markdownForm: FormGroup;

    @ViewChild('marComponet', {read: ElementRef}) marComponet: ElementRef;

    constructor(private formBuilder: FormBuilder,
                private fileService: FilesService,
                private router: Router,
                private activatedRoute: ActivatedRoute,
                private toastr: ToastrService,
                private spinner: NgxSpinnerService) {

        this.createForm();
    }

    createForm() {
        this.markdownForm = this.formBuilder.group({
            id: null,
            fileName: ['', Validators.required],
            content: ['', Validators.required],
        });
    }

    ngOnInit(): void {
        //  this.fileService.getFiles().subscribe(
        //     (result) => {
        //         this.files = result;
        //     },
        //     (error: HttpErrorResponse) => {
        //         console.log('Error: ==> ', error);
        //     }
        // );

        this.fileService.getAllDocuments().subscribe(
            (result) => {
                this.documents = result;
            },
            (error: HttpErrorResponse) => {
                console.log('Error: ==> ', error);
            }
        );
    }

    /**
     * 
     * @param formValue 
     */
    saveUpdate(formValue: FormGroup) {

        if (formValue.valid) {
            const markDown: MarkDown = formValue.getRawValue();

            // console.log(file);

            this.spinner.show();

            if (!markDown.id) {
                this.fileService.save(markDown).subscribe(
                    (result) => {
                        // this._spinner.hide();
                        this.documents = result;
                        this.markdownForm.reset();
                        this.marComponet.nativeElement.textContent = '';
                        this.toastr.success('Record saved successfully');
                        this.spinner.hide();
                    },
                    (error: HttpErrorResponse) => {
                        console.log('error', error);
                        this.spinner.hide();
                        // this._spinner.hide();
                        // this._toastr.error(JSON.stringify(error.error), 'Error');
                    }
                );
            } else {
                this.fileService.update(markDown).subscribe(
                    (result) => {
                        this.documents = result;
                        this.markdownForm.reset();
                        this.marComponet.nativeElement.textContent = '';

                        this.toastr.success('Record updated successfully');
                        this.spinner.hide();
                    },
                    (error: HttpErrorResponse) => {
                        console.log('error', error);
                        this.spinner.hide();
                    }
                );
            }
        }
    }

    /**
     * 
     */
    getFileDetail(document: MarkDown) {
        this.markdownForm.get('id').setValue(document.id);
        this.markdownForm.get('fileName').setValue(document.fileName);
        this.markdownForm.get('content').setValue(document.content);
    }

    /**
     * 
     */
    newFile() {
       this.markdownForm.reset();
       this.marComponet.nativeElement.textContent = '';

        // this.createForm();
        // this.markdownForm.get('content').setValue('');
    }

    /**
     * 
     */
    deleteFile(markDown: MarkDown) {
        this.spinner.show();

        this.fileService.delete(markDown.id).subscribe(
            (result) => {
                // this._spinner.hide();
                this.documents = result;
                this.markdownForm.reset();
                this.marComponet.nativeElement.textContent = '';

                this.toastr.success('Record deleted successfully');
                this.spinner.hide();
            },
            (error: HttpErrorResponse) => {
                console.log('error', error);
                this.spinner.hide();
                this.toastr.error(JSON.stringify(error.error), 'Error');
            }
        );
    }
}
