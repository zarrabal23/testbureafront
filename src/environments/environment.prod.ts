export const environment = {
  production: true,

  BASE_URL: 'http://localhost:8045/',
  API_URL: 'http://localhost:8045/api/v1/',

  // SERVICES ROUTES
  URL_FILES: 'files/',

  // AUTHENTICATION
};
